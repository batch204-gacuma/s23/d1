let myPokemon = {

	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,

	tackle: function() {
		console.log(this.name + ' tackle enemy')
		console.log("targetPokemon's health is low")
	},

	faint: function() {
		console.log('pokemon fainted.')
	}
}

console.log(myPokemon);

function Pokemon(name, level, health) {

	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = level;

	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s is now reduced to " + (target.health - this.attack))
		target.health=(target.health - this.attack);
				if (target.health <= 5) {
				target.faint();
			}

	},

	this.faint = function() {
		console.log(this.name+" fainted")
	}

}

let squirtle = new Pokemon ('squirtle', 100, 200);
let snorlax = new Pokemon ('snorlax', 90, 150)

snorlax.tackle(squirtle);
snorlax.tackle(squirtle);
snorlax.tackle(squirtle);
